# PIE-Office GUI: A GUI script converter for ancient (Proto-)Indo-European languages.

This application is a GUI version of my previous `pieoffice` for those who are more into normal applications.
I hope it is easy to use in Windows and Mac platforms (I would be glad if someone tries and send me some feedback, please do so).
I strongly suggest having the Noto font family installed in your system, covering all the wanted scripts, otherwise some signs might become a little bit funky.
Nevertheless, if you copy and paste to a well setup working environment with the proper font support, it should work like a charm.

So far, the mappings cover:
 - Proto-Indo-European
 - Indic:
    - Vedic / Sanskrit:
        - Devanagari
        - IAST
 - Iranic:
     - Avestan:
         - Script 
         - Transliterated 
     - Old Persian Cuneiform 
 - Celtic:
     - Ogham Script 
 - Italic:
     - Oscan Script 
 - Germanic:
     - Gothic Script 
 - Armenian Script 
 - Greek:
    - Polytonic Greek 
    - Mycenaean Linear B Script 
    - Cypriot Syllabary 
 - Anatolian:
    - Hieroglyphic Luwian 
    - Lydian 
    - Lycian 
    - Carian 

# Installation

The easiest way so far is, if you have pip, to run:

```bash
pip install --user pieoffice_gui
```

And to upgrade:

```bash
pip install --upgrade pieoffice_gui
```

# Usage

Simply run:

```bash
pieoffice_gui
```

And follow your intuition!

# TODO

Some rules are poorly diagrammed and might be displayed awkwardly, I should find a way to render them correctly across multiples platforms.
